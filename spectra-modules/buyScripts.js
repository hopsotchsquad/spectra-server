"use strict";

module.exports = {
	ca: function (user, room) {
		user.addItem("avatartoken", 1);
		return;
	},
	icon: function (user, room) {
		user.addItem("icontoken", 1);
		return;
	},
	color: function (user, room) {
		user.addItem("colortoken", 1);
		return;
	},
	symbol: function (user, room) {
		user.addItem("symboltoken", 1);
		return;
	},
	music: function (user, room) {
		user.addItem("musictoken", 1);
		return;
	},
	infobox: function (user, room) {
		user.addItem("infoboxtoken", 1);
		return;
	},
	battleintro: function (user, room) {
		user.addItem("battleintrotoken", 1);
		return;
	},
	room: function (user, room) {
		user.addItem("roomtoken", 1);
		return;
	},
	fix: function (user, room) {
		user.addItem("fixtoken", 1);
		return;
	},
	eabadge: function (user, room) {
		user.addItem("earlyadopterbadge", 1);
		return;
	},
	doublexp: function (user, room) {
		user.addItem("doublexpvoucher", 1);
		return;
	},
};
