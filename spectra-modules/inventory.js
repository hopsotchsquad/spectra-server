"use strict";

module.exports = class {
	constructor() {
		this.data = null;
		this.itemLib = null;
		this.enabled = false;
	}

	init() {
		const tasks = [];
		tasks.push(new Promise((resolve, reject) => {
			Spectra.loadJSON('spectra-data/inv.json', (err, data) => {
				if (err) {
					crash("Spectra Load-Inv error", err);
					resolve(0);
				} else {
					this.data = data;
					resolve(1);
				}
			});
		}));

		tasks.push(new Promise((resolve, reject) => {
			try {
				this.itemLib = require("./itemLib.js");
				resolve(1);
			} catch (e) {
				crash("Spectra Load ItemLib Error", e);
				resolve(0);
			}
		}));

		Promise.all(tasks).then(status => {
			if (status[0] + status[1] === 2) this.enabled = true;

			Spectra.UserClass.prototype.inventory = {};

			Spectra.UserClass.initFunctions.push(function (callback) {
				this.inventory = Inv.data[this.userid] || {};
				return callback();
			});

			Spectra.UserClass.prototype.removeItem = function (id, quantity) {
				if (this.inventory[id]) {
					this.inventory[id] = this.inventory[id] - quantity;
					if (this.inventory[id] <= 0) delete this.inventory[id];
				}

				Inv.data[this.userid] = this.inventory;
				Inv.update();
			};

			Spectra.UserClass.removeItem = function (id, quantity, userid) {
				if (!userid) return;
				if (Inv.data[userid]) {
					Inv.data[userid] = {};
					return;
				}

				if (Inv.data[userid][id]) {
					Inv.data[userid][id] = Inv.data[userid][id] - quantity;
					if (Inv.data[userid][id] <= 0) delete Inv.data[userid][id];
				}

				Inv.update();
			};

			Spectra.UserClass.prototype.addItem = function (id, quantity) {
				if (this.inventory[id]) {
					this.inventory[id] = this.inventory[id] + quantity;
				} else {
					this.inventory[id] = quantity;
				}

				Inv.data[this.userid] = this.inventory;
				Inv.update();
			};

			Spectra.UserClass.addItem = function (id, quantity, userid) {
				if (!userid) return;
				if (Inv.data[userid]) Inv.data[userid] = {};
				if (Inv.data[userid][id]) {
					Inv.data[userid][id] = Inv.data[userid][id] + quantity;
				} else {
					Inv.data[userid][id] = quantity;
				}

				Inv.update();
			};
		});

		return this;
	}

	sortList(userid, flag, invert) {
		let item;
		const sorted = new Set();
		if (!this.data[userid]) {
			this.data[userid] = {};
			this.update();
			return sorted;
		}
		for (const i in this.data[userid]) {
			item = this.itemLib[i];

			if (!item) continue;

			switch (flag) {
			case "usable":
				if (item.use) {
					sorted.add(i);
				} else if (invert) {
					sorted.add(i);
				}
				break;
			case "tradeable":
				if (item.tradeable) {
					sorted.add(i);
				} else if (invert) {
					sorted.add(i);
				}
				break;
			case "consumable":
				if (item.consumable) {
					sorted.add(i);
				} else if (invert) {
					sorted.add(i);
				}
				break;
			default:
				sorted.add(i);
				//Ignores invert
			}
		}

		return sorted;
	}

	update() {
		Spectra.writeJSON('spectra-data/inv.json', JSON.stringify(this.data), err => {
			if (err) crash("Spectra Save Inv error", err);
		});
	}
};
