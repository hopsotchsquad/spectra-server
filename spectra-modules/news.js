"use strict";

const moment = require("moment");

module.exports = class {
	constructor() {
		this.data = null;
		this.MAX_DISP = 6;
	}

	init() {
		Spectra.loadJSON('spectra-data/news.json', (err, data) => {
			if (err) crash("Load News Error", err, true);
			this.data = data;

			Spectra.UserClass.initFunctions.push(function (callback) {
				if (this.seenNews) return callback();
				News.check(this.userid, bool => {
					this.dispNews = bool;
					if (bool && this.connected) this.displayNews();
					if (bool && this.can('lock') && this.connected) this.displayStaffNews();
					this.seenNews = true;
					return callback();
				});
			});

			Spectra.UserClass.prototype.displayNews = function () {
				const listHTML = News.getNews(News.MAX_DISP);
				let output = "";
				for (let u in listHTML) output += `<div class="newsentry">${listHTML[u]}</div>`;
				if (listHTML) this.send(`|news| Server Updates|${output}`);
			};

			Spectra.UserClass.prototype.displayStaffNews = function () {
				if (!this.can('lock')) return;
				const listHTML = News.getStaffNews(News.MAX_DISP);
				let output = "";
				for (let u in listHTML) output += `<div class="newsentry">${listHTML[u]}</div>`;
				if (listHTML) this.send(`|news| Staff Updates|${output}`);
			};

			Spectra.UserClass.prototype.toggleNews = function () {
				const val = this.dispNews ? 0 : 1;
				database.run("UPDATE users SET news=$val WHERE userid=$userid", {
					$userid: this.userid,
					$val: val,
				}, err => {
					if (err) crash("Toggle News Error", err);
				});

				return val === 0;
			};
		});

		return this;
	}

	check(userid, callback) {
		database.all("SELECT news FROM users WHERE userid=$userid", {
			$userid: userid,
		}, (err, results) => {
			if (err) {
				crash("Check news bool Err", err);
				return callback(false);
			} else if (!results || !results[0] || !results[0].news) {
				return callback(false);
			} else {
				return callback(results[0].news === 1);
			}
		});
	}

	getNews(max) {
		let map = new Map(Object.entries(this.data));
		let entries = [];
		let highest;
		while (entries.length < max && map.size > 0) {
			for (const [i, entry] of map) {
				if (entry.staff) {
					map.delete(i);
					continue;
				}
				if (!highest || entry.time > highest.time) highest = entry;
			}
			entries.push(highest);
			map.delete(highest.id);
		}

		if (entries.length === 0) return null;

		const list = entries.reverse();
		const listHTML = [];

		for (const i of list) {
			listHTML.push(`<h4>${i.title}</h4>${i.text}<br><br><b>${i.type}</b> - ${moment(i.time).format("MMM D, YYYY")}`);
		}

		return listHTML;
	}

	getStaffNews(max) {
		let map = new Map(Object.entries(this.data));
		let entries = [];
		let highest;
		while (entries.length < max && map.size > 0) {
			for (const [i, entry] of map) {
				if (!entry.staff) {
					map.delete(i);
					continue;
				}
				if (!highest || entry.time > highest.time) highest = entry;
			}
			if (highest) {
				entries.push(highest);
				map.delete(highest.id);
			}
		}

		if (entries.length === 0) return null;

		const list = entries.reverse();
		const listHTML = [];

		for (const i of list) {
			listHTML.push(`<h4>${i.title}</h4>${i.text}<br><br>By <b>${i.by}</b> on ${moment(i.time).format("MMM D, YYYY")}`);
		}

		return listHTML;
	}

	addNews(parts) {
		let newsObj = {
			id: Spectra.genId(),
			title: parts[0],
			text: parts[1],
			type: parts[2],
			time: Date.now(),
		};

		this.data[newsObj.id] = newsObj;
		this.update();
		return true;
	}

	addStaffNews(parts, by) {
		let newsObj = {
			staff: true,
			id: Spectra.genId(),
			title: parts[0],
			text: parts[1],
			by: by,
			time: Date.now(),
		};

		this.data[newsObj.id] = newsObj;
		this.update();
		return true;
	}

	deleteNews(target) {
		if (!this.data[target]) return "The update you're looking for doesn't exist.";
		if (this.data[target].staff) return "The update you've selected is a staff update.";
		delete this.data[target];
		this.update();
		return true;
	}

	deleteStaffNews(target) {
		if (!this.data[target]) return "The update you're looking for doesn't exist.";
		if (!this.data[target].staff) return "The update you've selected is not a staff update.";
		delete this.data[target];
		this.update();
		return true;
	}

	update() {
		Spectra.writeJSON('spectra-data/news.json', JSON.stringify(this.data), err => {
			if (err) crash("Update News Err", err);
		});
	}
};
