"use strict";

//All the database abstraction happens here
const Abstractor = require("./lib/dbAbstractor.js");

//This is just a wrapper for ease of use
module.exports = class {
	constructor() {
		this.root = "usercards";
		this.abstractor = null;
		this.ready = false; //Dont let request be made until this is true
	}

	init() {
		this.abstractor = new Abstractor();
		this.abstractor.on("ready", () => {
			this.ready = true;
			Spectra.emit("cards");
		});
		this.abstractor.init(this.root);
		return this;
	}

	get(userid) {
		if (!this.ready) return null;
		return this.abstractor.request(userid);
	}

	add(userid, cardid, quantity) {
		if (!this.ready) return null;
		return this.abstractor.update(userid, "add", [cardid, quantity]);
	}

	take(userid, cardid, quantity) {
		if (!this.ready) return null;
		return this.abstractor.update(userid, "decrease", [cardid, quantity]);
	}

	takeAll(userid) {
		if (!this.ready) return null;
		return this.abstractor.update(userid, "clear", [null]);
	}

	transfer(u1, u2, c, q) {
		if (!this.ready) return null;
		return this.abstractor.move(u1, [c, q], u2, [c]);
	}

	transferAll(u1, u2) {
		if (!this.ready) return null;
		return this.abstractor.merge(u1, u2, {number: 1});
	}

	clean(userid, key) {
		if (!this.ready) return null;
		return this.abstractor.update(userid, "delete", [cardid]);
	}

	ban(userid) {
		if (!this.ready) return null;
		return this.abstractor.update(userid, "toplevel", ["banned"]);
	}

	unban(userid) {
		if (!this.ready) return null;
		return this.abstractor.update(userid, "toplevel", [{}]);
	}
}