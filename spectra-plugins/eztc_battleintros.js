/********************************
* EZ-TC Plugin by jd            *
* Makes adding trainer cards EZ *
********************************/

exports.commands = {
	eztc: 'trainercard',
	trainercards: 'trainercard',
	infobox: 'trainercard',
	tc: 'trainercard',
	trainercard: function (target, room, user) {
		if (!target) target = 'help';
		let parts = target.split(',');
		for (let u in parts) parts[u] = parts[u].trim();

		switch (parts[0]) {
		case 'add':
			if (!user.isUpperStaff) return false;
			if (!parts[2]) return this.sendReply("Usage: /eztc add, [command name], [html]");
			var id = toId(parts[1]);
			if (Chat.commands[id]) return this.sendReply("/eztc - The command \"" + id + "\" already exists.");
			const html = parts.splice(2, parts.length).join(',');
			Spectra.trainerCards[id] = new Function('target', 'room', 'user', "if (!room.disableTrainerCards) if (!this.runBroadcast()) return; this.sendReplyBox('" + html.replace(/'/g, "\\'") + "');");
			Spectra.writeTCs();
			this.sendReply("The Infobox \"" + id + "\" has been added.");
			this.logModCommand(user.name + " added the Infobox " + id);
			break;

		case 'rem':
		case 'del':
		case 'delete':
		case 'remove':
			if (!user.isUpperStaff) return false;
			if (!parts[1]) return this.sendReply("Usage: /eztc remove, [command name]");
			var id = toId(parts[1]);
			if (!Spectra.trainerCards[id]) return this.sendReply("/trainercards - The command \"" + id + "\" does not exist, or was added manually.");
			delete Chat.commands[id];
			delete Spectra.trainerCards[id];
			Spectra.writeTCs();
			this.sendReply("The Infobox \"" + id + "\" has been removed.");
			this.logModCommand(user.name + " removed the Infobox " + id);
			break;

		case 'list':
			if (!user.isUpperStaff) return false;
			let output = "<b>There's a total of " + Object.keys(Spectra.trainerCards).length + " Infoboxes added with this command:</b><br />";
			for (const tc in Spectra.trainerCards) {
				output += tc + "<br />";
			}
			this.sendReplyBox(output);
			break;

		case 'off':
			if (!this.can('declare')) return false;
			if (room.disableTrainerCards) return this.sendReply("Broadcasting Infoboxes is already disabled in this room.");
			room.disableTrainerCards = true;
			room.chatRoomData.disableTrainerCards = true;
			Rooms.global.writeChatRoomData();
			this.privateModCommand("(" + user.name + " has disabled broadcasting Infoboxes in this room.)");
			break;

		case 'on':
			if (!this.can('declare')) return false;
			if (!room.disableTrainerCards) return this.sendReply("Broadcasing Infoboxes is already enabled in this room.");
			delete room.disableTrainerCards;
			delete room.chatRoomData.disableTrainerCards;
			Rooms.global.writeChatRoomData();
			this.privateModCommand("(" + user.name + " has enabled broadcasting Infoboxes in this room.)");
			break;

		default:
		case 'info':
		case 'help':
			if (!this.runBroadcast()) return;
			this.sendReplyBox(
				"EZ-TC Commands:<br />" +
				"/eztc add, [command name], [html] - Adds an Infobox.<br />" +
				"/eztc remove, [command name] - Removes an Infobox.<br />" +
				"/eztc list - Shows a list of all Infoboxes added with this command.<br />" +
				"/eztc off - Disables broadcasting Infoboxes in the current room.<br />" +
				"/eztc on - Enables broadcasting Infoboxes in the current room.<br />" +
				"/eztc help - Shows this help command.<br />" +
				"<a href=\"https://gist.github.com/jd4564/399934fce2e9a5ae29ad\">EZ-TC Plugin by jd</a> (But Lights named it years ago)"
			);
		}
	},

	//fuck you Lights made this one fuck you jd i didnt copy paste your code fuck you ok

	battleintro: function (target, room, user) {
		if (!target) target = 'help';
		let parts = target.split(',');
		for (let u in parts) parts[u] = parts[u].trim();

		switch (parts[0]) {
		case 'add':
			if (!user.isUpperStaff) return false;
			if (!parts[2]) return this.sendReply("Usage: /battleintro add, [userid], [html]");
			var id = toId(parts[1]);
			const html = parts.splice(2, parts.length).join(',');
			Spectra.battleIntros[id] = html.replace(/'/g, "\\'");
			Spectra.writeBIs();
			this.sendReply("The Battle Intro for \"" + id + "\" has been added.");
			this.logModCommand(user.name + " added a Battle Intro for " + id);
			break;

		case 'rem':
		case 'del':
		case 'delete':
		case 'remove':
			if (!user.isUpperStaff) return false;
			if (!parts[1]) return this.sendReply("Usage: /battleintro remove, [userid]");
			var id = toId(parts[1]);
			if (!Spectra.battleIntros[id]) return this.sendReply("/battleintro - \"" + id + "\" does not have a Battle Intro.");
			delete Spectra.battleIntros[id];
			Spectra.writeBIs();
			this.sendReply("The Battle Intro for \"" + id + "\" has been removed.");
			this.logModCommand(user.name + " removed the Battle Intro for " + id);
			break;

		default:
		case 'info':
		case 'help':
			if (!this.runBroadcast()) return;
			this.sendReplyBox(
				"Battle Intro Commands:<br />" +
				"/battleintro add, [userid], [html] - Adds a Battle Intro.<br />" +
				"/battleintro remove, [command name] - Removes a Battle Intro.<br />" +
				"/battleintro help - Shows this help command.<br />" +
				"Blatant ripoff of jd's EZ-TC code."
			);
		}
	}
};