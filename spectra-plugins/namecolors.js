/* Custom color plugin
 * by jd and panpawn
 */
'use strict';

exports.commands = {
	customcolor: {
		set: function (target, room, user) {
			if (!this.can('rangeban')) return false;
			target = target.split(',');
			for (let u in target) target[u] = target[u].trim();
			if (!target[1]) return this.parse('/help customcolor');
			if (toId(target[0]).length > 19) return this.errorReply("Usernames are not this long...");

			this.sendReply("|raw|You have given <b><font color=" + target[1] + ">" + Chat.escapeHTML(target[0]) + "</font></b> a custom color.");
			Rooms('upperstaff').add('|raw|' + Chat.escapeHTML(target[0]) + " has recieved a <b><font color=" + target[1] + ">custom color</fon></b> from " + Chat.escapeHTML(user.name) + ".").update();
			Spectra.customColors[toId(target[0])] = target[1];
			Spectra.writeColors();
		},

		delete: function (target, room, user) {
			if (!this.can('rangeban')) return false;
			if (!target) return this.parse('/help customcolor');
			if (!Spectra.customColors[toId(target)]) return this.errorReply('/customcolor - ' + target + ' does not have a custom color.');
			delete Spectra.customColors[toId(target)];
			Spectra.writeColors();
			this.sendReply("You removed " + target + "'s custom color.");
			Rooms('upperstaff').add(user.name + " removed " + target + "'s custom color.").update();
			if (Users(target) && Users(target).connected) Users(target).popup(user.name + " removed your custom color.");
		},
		preview: function (target, room, user) {
			if (!this.runBroadcast()) return;
			target = target.split(',');
			for (let u in target) target[u] = target[u].trim();
			if (!target[1]) return this.parse('/help customcolor');
			return this.sendReplyBox('<b><font size="3" color="' + target[1] + '">' + Chat.escapeHTML(target[0]) + '</font></b>');
		},
		reload: function (target, room, user) {
			if (!Spectra.isOwner(user.userid)) return false;
			Spectra.writeColors();
			this.privateModCommand("(" + user.name + " has reloaded custom colours.)");
		},
		'': function (target, room, user) {
			return this.parse("/help customcolor");
		},
	},
	customcolorhelp: [
		"Commands Include:",
		"/customcolor set [user], [hex] - Gives [user] a custom color of [hex]",
		"/customcolor delete [user] - Deletes a user's custom color",
		"/customcolor reload - Reloads colours.",
		"/customcolor preview [user], [hex] - Previews what that username looks like with [hex] as the color.",
	],

	customcolour: function (target, room, user) {
		return this.errorReply("Fuck you, its spelled 'color' not 'colour'.");
	},
};
