"use strict";

const fs = require("fs");
const path = require("path");
const allowedFormats = ['png', 'jpg', 'jpeg', 'gif'];

const cmds = {
	'': 'help',
	help: function (target, room, user) {
		if (!this.canBroadcast()) return;
		return this.sendReplyBox('<b>Custom Avatar commands</b><br>' +
			'(All commands require &)<br><br>' +
			'<li>/ca set <small>or</small> /setavatar <em>User</em>, <em>URL</em> - Sets a user\'s custom avatar to the specified image URL.' +
			'<li>/ca delete <small>or</small> /deleteavatar <em>User</em> - Deletes a user\'s custom avatar.'
		);
	},

	add: 'set',
	set: function (target, room, user, connection, cmd) {
		if (!this.can('rangeban')) return this.sendReply('/ca add - Access Denied.');
		if (!target) return this.sendReply('|html|/ca set <em>User</em>, <em>URL</em> - Sets a user\'s custom avatar to the specified image.');
		target = target.split(',');
		if (target.length < 2) return this.sendReply('|html|/ca set <em>User</em>, <em>URL</em> - Sets a user\'s custom avatar to the specified image.');
		let targetUser = Users(target[0]) ? Users(target[0]).name : target[0];
		let link = target[1].trim();
		if (!link.match(/^https?:\/\//i)) link = 'http://' + link;

		new Promise((resolve, reject) => {
			require("request").get(link).on('error', err => {
				if (err) crash("Get Avy err", err);
				reject("Avatar Unavailable.");
			}).on('response', response => {
				if (response.statusCode !== 200) reject("Avatar Unavailable.");
				const type = response.headers['content-type'].split("/");
				if (type[0] !== 'image') reject("Link is not an image link.");
				if (!~allowedFormats.indexOf(type[1])) reject("Format not supported. Supported formats include: " + allowedFormats.join(", "));

				if (Spectra.customAvatars[toId(targetUser)] && fs.existsSync(`config/avatars/${Spectra.customAvatars[toId(targetUser)]}`)) fs.unlinkSync(`config/avatars/${Spectra.customAvatars[toId(targetUser)]}`);
				const file = `${toId(targetUser)}.${type[1]}`;
				response.pipe(fs.createWriteStream(`config/avatars/${file}`));
				resolve(file);
			});
		}).then(file => {
			Spectra.customAvatars[toId(targetUser)] = file;
			const targUser = Users(toId(targetUser));
			if (targUser) targUser.avatar = file;

			const text = `custom avatar has been set to <br><div style = "width: 80px; height: 80px; display: block"><img src = "${link}" style = "max-height: 100%; max-width: 100%"></div>`;
			this.sendReply(`|html|${targetUser}'s ${text}`);
			if (targUser) {
				targUser.send('|html|Your custom avatar has been set. Refresh your page if you don\'t see it.');
				targUser.popup(`|html|<center>Your ${text}<br>Refresh your page if you don't see it.</center>`);
			}
		}).catch(err => {
			this.errorReply("Unable to set Avatar: Err: " + err);
		});
	},

	remove: 'delete',
	'delete': function (target, room, user, connection, cmd) {
		if (!target || !target.trim()) return this.sendReply('|html|/' + cmd + ' <em>User</em> - Deletes a user\'s custom avatar.');
		target = Users.getExact(target) ? Users.getExact(target).name : target;
		if (Spectra.customAvatars[toId(target)] && fs.existsSync(`config/avatars/${Spectra.customAvatars[toId(target)]}`)) return this.errorReply(target + ' does not have a custom avatar.');

		fs.unlinkSync('config/avatars/' + Spectra.customAvatars[toId(target)]);
		delete Spectra.customAvatars[toId(target)];
		this.sendReply(target + '\'s custom avatar has been successfully removed.');
		if (Users.getExact(target)) {
			Users.getExact(target).send('Your custom avatar has been removed.');
			Users.getExact(target).avatar = 1;
		}
	},
};

exports.commands = {
	ca: 'customavatar',
	customavatar: cmds,
	deleteavatar: 'removeavatar',
	removeavatar: cmds.delete,
	setavatar: cmds.set,
};
