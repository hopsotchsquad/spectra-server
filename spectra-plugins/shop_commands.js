"use strict";

const fs = require('fs');

exports.commands = {
	shop: function (target, room, user) {
		const key = target ? toId(target).trim() : "";
		if (key === "help") {
			return this.sendReplyBox("/shop - opens shop UI<br/>/receipts - lists your recent receipts<br/>/receipt [receipt ID] - displays the details of the specified receipt if you are the owner<br/>/receipts view, [user] - lists receipts of the specified user (requires @)<br/>Multi-Shop overhaul by Lights");
		} else if (key === "back") {
			if (!user.shop || !user.shop.last || !user.shop.last.length || user.shop.last.length === 0) return;
			return user.shop.back();
		} else if (key === "") {
			user.shopCounter++;
			user.shop = Shop.spawnInstance(user, room);
			return user.shop.draw();
		} else if (key === "exit") {
			if (!user.shop) return;
			user.shop.exit();
			delete user.shop;
			return user.connections[0].sendTo(room.id, `|uhtmlchange|${user.userid}shop${user.shopCounter}|`);
		} else {
			if (!user.shop) return;
			return user.shop.select(target);
		}
	},

	redeem: function (target, room, user) {
		const err = "Invalid Item ID - This command is not designed to be used manually.";
		if (!target) return this.errorReply(err);
		const item = user.shop.getItem(target ? toId(target).trim() : "");
		if (!item) return this.errorReply(err);

		Shop.runTransaction(user, item, room);
	},

	receipt: 'receipts',
	receipts: function (target, room, user) {
		let options;
		const receipts = fs.readFileSync('logs/transactions.log', 'utf8').split('\n').reverse();
		if (!target) {
			options = 1;
		} else {
			target = target.split(',');
			if (target.length > 1) {
				options = 2;
				for (let t in target) target[t] = toId(target[t]).trim();
			} else {
				target = target[0].trim();
				if (target === 'help') return this.parse('/shop help');
				options = 3;
			}
		}
		if (options === 1) {
			let ownedReceipts = [];
			for (let i in receipts) {
				let parts = receipts[i].split('|');
				if (toId(parts[0]) === user.userid) ownedReceipts.push(parts[5]);
			}
			if (ownedReceipts.length > 0) {
				return this.sendReply("Your receipts: " + ownedReceipts.join(', ') + ". Use /receipt [receiptID] to view details.");
			} else {
				return this.sendReply("You do not have any recent receipts (receipts are deleted after one month).");
			}
		} else if (options === 2) {
			if (target[0] !== 'view' || target.length !== 2) this.sendReply("/receipts view, [user]");
			if (!this.can('ban')) this.errorReply("Access Denied.");
			let name = Users.get(target[1]);
			if (!name) {
				name = target[1];
			} else {
				name = name.name;
			}
			let ownedReceipts = [];
			for (let i in receipts) {
				let parts = receipts[i].split('|');
				if (toId(parts[0]) === target[1]) ownedReceipts.push(parts[5]);
			}
			if (ownedReceipts.length > 0) {
				return this.sendReply("Receipts of " + name + ": " + ownedReceipts.join(', ') + ". Use /receipt [receiptID] to view details.");
			} else {
				return this.sendReply(name + " does not have any recent receipts (receipts are deleted after one month).");
			}
		} else if (options === 3) {
			let receipt = false;
			for (let i in receipts) {
				let parts = receipts[i].split('|');
				if (parts[5] === target) receipt = parts;
			}
			if (!receipt) {
				return this.errorReply("Invalid receipt ID");
			} else {
				if (receipt[0] !== user.userid && !this.can('ban')) return this.sendReply("You can only view your own receipts.");
				let expire = parseInt(receipt[7]);
				expire = +30 - +expire;
				expire = expire.toString();
				return this.sendReplyBox('<center><b>Receipt ' + receipt[5] + '</b></center><br/><b>Name: </b>' + receipt[0] + '<br/><b>Date: </b>' + receipt[1] + '<br/><b>Item: </b>' + receipt[2] + '<br/><b>Price: </b>' + receipt[3] + '<br/><b>Currency: </b>' + receipt[4] + '<br/><b>Remaining ' + receipt[4] + ': </b>' + receipt[6] + '<br/><b>Days until expiry: </b>' + expire);
			}
		} else {
			return this.parse("/shop help");
		}
	},
};
