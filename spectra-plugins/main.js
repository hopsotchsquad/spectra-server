"use strict";

//Misc custom command dump, temp file

const moment = require('moment');

let messages = [
	"has vanished into nothingness!",
	"used Explosion!",
	"fell into the void.",
	"went into a cave without a repel!",
	"has left the building.",
	"was hit by Magikarp's Revenge!",
	"ate a bomb!",
	"is blasting off again!",
	"(Quit: oh god how did this get here i am not good with computer)",
	"was unfortunate and didn't get a cool message.",
	"{{user}}'s mama accidently kicked {{user}} from the server!",
];

exports.commands = {
	showauth: 'hideauth',
	show: 'hideauth',
	hide: 'hideauth',
	hideauth: function (target, room, user, connection, cmd) {
		if (!user.can('rangeban')) return this.sendReply("/hideauth - access denied.");
		if (cmd === 'show' || cmd === 'showauth') {
			delete user.rankDisplay;
			user.updateIdentity();
			return this.sendReply("You have revealed your auth symbol.");
		}
		let tar = ' ';
		if (target) {
			target = target.trim();
			if (Config.groupsranking.indexOf(target) > -1 && target !== '#') {
				if (Config.groupsranking.indexOf(target) <= Config.groupsranking.indexOf(user.group)) {
					tar = target;
				} else {
					this.sendReply('The group symbol you have tried to use is of a higher authority than you have access to. Defaulting to \' \' instead.');
				}
			} else {
				this.sendReply('You have tried to use an invalid character as your auth symbol. Defaulting to \' \' instead.');
			}
		}
		user.rankDisplay = tar;
		user.updateIdentity();
		this.sendReply('You are now hiding your auth symbol as \'' + tar + '\'.');
		this.logModCommand(user.name + ' is hiding auth symbol as \'' + tar + '\'');
	},

	clearroom:  function (target, room, user) {
		if (!this.can('rangeban', null, room)) return false;
		if (room.battle) return this.sendReply("You cannot clearall in battle rooms.");
		let len = room.log.length;
		let users = [];
		while (len--) {
			room.log[len] = '';
		}
		for (let u in room.users) {
			users.push(u);
			Users.get(u).leaveRoom(room, Users.get(u).connections[0]);
		}
		len = users.length;
		setTimeout(function () {
			while (len--) {
				Users.get(users[len]).joinRoom(room, Users.get(users[len]).connections[0]);
			}
		}, 1000);
	},

	roomkick: 'kick',
	kick: function (target, room, user) {
		if (!target) return this.parse('/help kick');
		if (!this.canTalk()) return false;
		target = this.splitTarget(target);
		let targetUser = this.targetUser;
		if (!targetUser || !targetUser.connected) {
			return this.errorReply('User "' + this.targetUsername + '" not found.');
		}
		if (!this.can('mute', targetUser, room)) return false;
		if (!(targetUser in room.users)) return this.errorReply("User '" + targetUser + "' is not in this room.");
		this.addModCommand(targetUser.name + ' was kicked from the room by ' + user.name + '.');
		targetUser.popup('You were kicked from ' + room.id + ' by ' + user.name + '.');
		targetUser.leaveRoom(room.id);
	},
	kickhelp: ['/kick [user] - Kicks a user from the room.'],
	roomkickhelp: ['/kick [user] - Kicks a user from the room.'],

	deroomauthall: function (target, room, user) {
		if (!this.can('root', null, room)) return false;
		if (!room.auth) return this.errorReply("Room does not have any roomauth to delete.");
		if (!target) {
			user.lastCommand = '/deroomauthall';
			this.errorReply("ALL ROOMAUTH WILL BE GONE, ARE YOU SURE?");
			this.errorReply("To confirm, use: /deroomauthall confirm");
			return;
		}
		if (user.lastCommand !== '/deroomauthall' || target !== 'confirm') {
			return this.parse('/help deroomauthall');
		}
		let count = 0;
		for (let userid in room.auth) {
			if (['+', '$', '%', '@', '&', '#'].includes(room.auth[userid])) {
				delete room.auth[userid];
				if (userid in room.users) room.users[userid].updateIdentity(room.id);
				count++;
			}
		}
		if (!count) {
			return this.sendReply("(This room has no roomauth)");
		}
		if (room.chatRoomData) {
			Rooms.global.writeChatRoomData();
		}
		this.addModCommand("All " + count + " roomauth has been cleared by " + user.name + ".");
	},
	deroomauthallhelp: ["/deroomauthall - Deauths all roomauthed users. Requires: ~"],

	userid: function (target, room, user) {
		if (!target) return this.sendReply('You must specify a user');
		target = this.splitTarget(target);
		let targetUser = this.targetUser;
		this.sendReply('The userid of this.targetUser is ' + targetUser);
	},

	alert: function (target, room, user) {
		if (user.can('rangeban')) {
			target = this.splitTarget(target);
			let targetUser = this.targetUser;
			if (!targetUser) return this.sendReply('You need to specify a user.');
			if (!target) return this.sendReply('You need to specify a message to be sent.');
			targetUser.popup(target);
			this.sendReply(targetUser.name + ' successfully recieved the message: ' + target);
			targetUser.send(user.name + ' sent you a popup alert.');
			this.addModCommand(user.name + ' sent a popup alert to ' + targetUser.name);
		}
	},

	forcelogout: function (target, room, user) {
		if (user.can('hotpatch')) {
			if (!target) return this.sendReply('You must specify a target.');
			target = this.splitTarget(target);
			let targetUser = this.targetUser;
			if (targetUser.can('hotpatch')) return this.sendReply('You cannot logout an Administrator.');
			this.addModCommand(targetUser + ' was forcibly logged out by ' + user.name + '.');
			targetUser.resetName();
		}
	},

	roomlist: function (target, room, user) {
		let header = ['<b><font color="#1aff1a" size="2">Total users connected: ' + Rooms.global.userCount + '</font></b><br />'],
			official = ['<b><font color="#ff9900" size="2"><u>Official Rooms:</u></font></b><br />'],
			nonOfficial = ['<hr><b><u><font color="#005ce6" size="2">Public Rooms:</font></u></b><br />'],
			privateRoom = ['<hr><b><u><font color="#ff0066" size="2">Private Rooms:</font></u></b><br />'],
			groupChats = ['<hr><b><u><font color="#00b386" size="2">Group Chats:</font></u></b><br />'],
			battleRooms = ['<hr><b><u><font color="#cc0000" size="2">Battle Rooms:</font></u></b><br />'];

		let rooms = [];

		Rooms.rooms.forEach(curRoom => {
			if (curRoom.id !== 'global') rooms.push(curRoom.id);
		});

		rooms.sort();

		for (let u in rooms) {
			let curRoom = Rooms(rooms[u]);
			if (curRoom.modjoin) {
				if (Config.groupsranking.indexOf(curRoom.modjoin) > Config.groupsranking.indexOf(user.group)) continue;
			}
			if (curRoom.isPrivate === true && !user.can('roomowner')) continue;
			if (curRoom.type === 'battle') {
				battleRooms.push('<a href="/' + curRoom.id + '" class="ilink">' + Chat.escapeHTML(curRoom.title) + '</a> (' + curRoom.userCount + ')');
			}
			if (curRoom.type === 'chat') {
				if (curRoom.isPersonal) {
					groupChats.push('<a href="/' + curRoom.id + '" class="ilink">' + curRoom.id + '</a> (' + curRoom.userCount + ')');
					continue;
				}
				if (curRoom.isOfficial) {
					official.push('<a href="/' + toId(curRoom.title) + '" class="ilink">' + Chat.escapeHTML(curRoom.title) + '</a> (' + curRoom.userCount + ')');
					continue;
				}
				if (curRoom.isPrivate) {
					privateRoom.push('<a href="/' + toId(curRoom.title) + '" class="ilink">' + Chat.escapeHTML(curRoom.title) + '</a> (' + curRoom.userCount + ')');
					continue;
				}
			}
			if (curRoom.type !== 'battle') nonOfficial.push('<a href="/' + toId(curRoom.title) + '" class="ilink">' + curRoom.title + '</a> (' + curRoom.userCount + ')');
		}

		if (!user.can('lock')) return this.sendReplyBox(header + official.join(' ') + nonOfficial.join(' '));
		this.sendReplyBox(header + official.join(' ') + nonOfficial.join(' ') + privateRoom.join(' ') + (groupChats.length > 1 ? groupChats.join(' ') : '') + (battleRooms.length > 1 ? battleRooms.join(' ') : ''));
	},

	plock: function (target, room, user, connection, cmd) {
		if (!this.can('declare')) return false;
		if (!target) return this.parse('/help pban');
		target = this.splitTarget(target);
		let targetUser = this.targetUser;
		if (!targetUser) return this.errorReply("User '" + this.targetUsername + "' not found.");
		if (target.length > 300) {
			return this.errorReply("The reason is too long. It cannot exceed " + 300 + " characters.");
		}
		if (!this.can('lock', targetUser)) return false;
		let name = targetUser.getLastName();
		let userid = targetUser.getLastId();
		if (Punishments.getPunishType(userid) === 'LOCKED' && !target && !targetUser.connected) {
			let problem = " but was already locked";
			return this.privateModCommand("(" + name + " would be permanently banned by " + user.name + problem + ".)");
		}
		if (targetUser.confirmed) {
			let from = targetUser.deconfirm();
			Monitor.log("[CrisisMonitor] " + name + " was permanently banned by " + user.name + " and demoted from " + from.join(", ") + ".");
			this.globalModlog("CRISISDEMOTE", targetUser, " from " + from.join(", "));
		}
		// Destroy personal rooms of the banned user.
		for (let i in targetUser.roomCount) {
			if (i === 'global') continue;
			let targetRoom = Rooms.get(i);
			if (targetRoom.isPersonal && targetRoom.auth[userid] && targetRoom.auth[userid] === '#') {
				targetRoom.destroy();
			}
		}
		targetUser.popup("|modal|" + user.name + " has permanently banned you." + (target ? "\n\nReason: " + target : "") + (Config.appealurl ? "\n\nIf you feel that your lock was unjustified, you can appeal:\n" + Config.appealurl : "") + "\n\nYour lock is permanent.");
		this.addModCommand("" + name + " was permanently banned by " + user.name + "." + (target ? " (" + target + ")" : ""), " (" + targetUser.latestIp + ")");
		let alts = targetUser.getAltUsers();
		let acAccount = (targetUser.autoconfirmed !== userid && targetUser.autoconfirmed);
		if (alts.length) {
			let guests = alts.length;
			alts = alts.filter(alt => alt.substr(0, 7) !== '[Guest ');
			guests -= alts.length;
			this.privateModCommand("(" + name + "'s " + (acAccount ? " ac account: " + acAccount + ", " : "") + "banned alts: " + alts.join(", ") + (guests ? " [" + guests + " guests]" : "") + ")");
			for (let i = 0; i < alts.length; ++i) {
				this.add('|unlink|' + toId(alts[i]));
			}
		} else if (acAccount) {
			this.privateModCommand("(" + name + "'s ac account: " + acAccount + ")");
		}

		this.add('|unlink|hide|' + userid);
		this.add('|uhtmlchange|' + userid + '|');
		if (userid !== toId(this.inputUsername)) {
			this.add('|unlink|hide|' + toId(this.inputUsername));
			this.add('|uhtmlchange|' + toId(this.inputUsername) + '|');
		}

		Punishments.lock(targetUser, 365 * 24 * 60 * 60 * 1000, null, target);
		this.globalModlog("PERMALOCK", targetUser, " by " + user.name + (target ? ": " + target : ""));
		return true;
	},
	plockhelp: ["/plock - Permanently locks a user."],

	pban: function (target, room, user, connection, cmd) {
		if (!this.can('rangeban')) return false;
		if (!target) return this.parse('/help pban');
		target = this.splitTarget(target);
		let targetUser = this.targetUser;
		if (!targetUser) return this.errorReply("User '" + this.targetUsername + "' not found.");
		if (target.length > 300) {
			return this.errorReply("The reason is too long. It cannot exceed " + 300 + " characters.");
		}
		if (!this.can('ban', targetUser)) return false;
		let name = targetUser.getLastName();
		let userid = targetUser.getLastId();
		if (Punishments.getPunishType(userid) === 'BANNED' && !target && !targetUser.connected) {
			let problem = " but was already banned";
			return this.privateModCommand("(" + name + " would be permanently banned by " + user.name + problem + ".)");
		}
		if (targetUser.confirmed) {
			let from = targetUser.deconfirm();
			Monitor.log("[CrisisMonitor] " + name + " was permanently banned by " + user.name + " and demoted from " + from.join(", ") + ".");
			this.globalModlog("CRISISDEMOTE", targetUser, " from " + from.join(", "));
		}
		// Destroy personal rooms of the banned user.
		for (let i in targetUser.roomCount) {
			if (i === 'global') continue;
			let targetRoom = Rooms.get(i);
			if (targetRoom.isPersonal && targetRoom.auth[userid] && targetRoom.auth[userid] === '#') {
				targetRoom.destroy();
			}
		}
		targetUser.popup("|modal|" + user.name + " has permanently banned you." + (target ? "\n\nReason: " + target : "") + (Config.appealurl ? "\n\nIf you feel that your ban was unjustified, you can appeal:\n" + Config.appealurl : "") + "\n\nYour ban is permanent.");
		this.addModCommand("" + name + " was permanently banned by " + user.name + "." + (target ? " (" + target + ")" : ""), " (" + targetUser.latestIp + ")");
		let alts = targetUser.getAltUsers();
		let acAccount = (targetUser.autoconfirmed !== userid && targetUser.autoconfirmed);
		if (alts.length) {
			let guests = alts.length;
			alts = alts.filter(alt => alt.substr(0, 7) !== '[Guest ');
			guests -= alts.length;
			this.privateModCommand("(" + name + "'s " + (acAccount ? " ac account: " + acAccount + ", " : "") + "banned alts: " + alts.join(", ") + (guests ? " [" + guests + " guests]" : "") + ")");
			for (let i = 0; i < alts.length; ++i) {
				this.add('|unlink|' + toId(alts[i]));
			}
		} else if (acAccount) {
			this.privateModCommand("(" + name + "'s ac account: " + acAccount + ")");
		}

		this.add('|unlink|hide|' + userid);
		this.add('|uhtmlchange|' + userid + '|');
		if (userid !== toId(this.inputUsername)) {
			this.add('|unlink|hide|' + toId(this.inputUsername));
			this.add('|uhtmlchange|' + toId(this.inputUsername) + '|');
		}

		Punishments.ban(targetUser, 365 * 24 * 60 * 60 * 1000, null, target);
		this.globalModlog("PERMABAN", targetUser, " by " + user.name + (target ? ": " + target : ""));
		LoginServer.request({
			act: "ban",
			username: toId(targetUser)
		});
		return true;
	},
	pbanhelp: ["/pban - Permanently bans a user."],

	tell: function (target, room, user, connection) {
		if (!target) return this.parse('/help tell');
		target = this.splitTarget(target);
		let targetUser = this.targetUser;
		if (!target) {
			this.sendReply("You forgot the comma.");
			return this.parse('/help tell');
		}

		if (targetUser && targetUser.connected) {
			return this.parse('/pm ' + this.targetUsername + ', ' + target);
		}

		if (user.locked) return this.popupReply("You may not send offline messages when locked.");
		if (target.length > 255) return this.popupReply("Your message is too long to be sent as an offline message (>255 characters).");

		if (Config.tellrank === 'autoconfirmed' && !user.autoconfirmed) {
			return this.popupReply("You must be autoconfirmed to send an offline message.");
		} else if (!Config.tellrank || Config.groupsranking.indexOf(user.group) < Config.groupsranking.indexOf(Config.tellrank)) {
			return this.popupReply("You cannot send an offline message because offline messaging is " +
			(!Config.tellrank ? "disabled" : "only available to users of rank " + Config.tellrank + " and above") + ".");
		}

		let userid = toId(this.targetUsername);
		if (userid.length > 18) return this.popupReply("\"" + this.targetUsername + "\" is not a legal username.");

		let sendSuccess = Tells.addTell(user, userid, target);
		if (!sendSuccess) {
			if (sendSuccess === false) {
				return this.popupReply("User " + this.targetUsername + " has too many offline messages queued.");
			} else {
				return this.popupReply("You have too many outgoing offline messages queued. Please wait until some have been received or have expired.");
			}
		}
		return connection.send('|pm|' + user.getIdentity() + '|' +
		(targetUser ? targetUser.getIdentity() : ' ' + this.targetUsername) +
		"|/text This user is currently offline. Your message will be delivered when they are next online.");
	},
	tellhelp: ["/tell [username], [message] - Send a message to an offline user that will be received when they log in."],
};
